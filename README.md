# Welcome to UmbracoScanner #

UmbracoScanner is a web application vulnerabilities scanner, for Umbraco-based apps. UmbracoScanner will tell you what is exposed to external users, and even tell you to replace a specific file due to its vulnerability.

### Why do we need UmbracoScanner? ###

Penetration Testers today have many tools to check different web applications. As far as I know, there's no real Umbraco-targeted scanner to ensure that the application , which is based on Umbraco, is safe.

We scan the whole Umbraco file-system, trying to find which files are public to external users.

In the next level, the scanner will be able to identify the version of Umbraco, If the web application uses a file from older version.

UmbracoScanner will support multi-threads for faster scanning, and also support delay for more safe scanning.

Help us build UmbracoScanner! Post your suggestions and issues and try to make UmbracoScanner even better!