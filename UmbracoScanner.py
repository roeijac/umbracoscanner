#!/usr/bin/python

# Umbraco Scanner
# Testing Umbraco-Based Website vulnerabilities
# By Roei Jacobovich

import urllib2
import platform
import os
import sys
import time
from Helper import Progress
from Helper import Arguments

prog = Progress()
arg = Arguments()


def checkFiles():
    lines6 = [line.rstrip('\n') for line in open('list6.2.5.txt')]
    lines4 = [line.rstrip('\n') for line in open('list4.11.10.txt')]
    intersection = set(lines6).symmetric_difference(lines4)


def progressBar():
    prog.refreshAnimation()
    sys.stdout.write('\r' + '' * len(prog.msg))
    sys.stdout.flush()
    prog.msg = '    # Scanning your site:      Scanned ' + str(prog.numScanned) + ' from ' + str(prog.numAll) \
          + '' + prog.strAnimation
    sys.stdout.write(prog.msg)
    sys.stdout.flush()


def printMessage(msg):
    sys.stdout.write('\r' + '' * len(prog.msg))
    sys.stdout.flush()
    sys.stdout.write(msg + '\n')
    sys.stdout.flush()

    # Now, file
    if hasattr(arg,'file'):
        arg.file.write(msg + '\n')

def scan(address):
    counter = 0
    # Loading lines from the list file
    lines = [line.rstrip('\n') for line in open('testlist.txt')]
    for line in lines:
        line.replace('\\','/')
    prog.numAll = len(lines)

    printMessage('[*] Scan has started... ')
    printMessage('[*] Scanning file-system of version 7.2.8... ')
    for key in lines:
        try:
            fnlAddr = address + key
            response = urllib2.urlopen(fnlAddr)
        except urllib2.HTTPError, e:
            if str(e) != 'HTTP Error 404: Not Found':
                if str(e) == 'HTTP Error 403: Forbidden':
                    if arg.argResult.forbidden: # Show forbidden results if user decided that
                        printMessage('[-] ' + fnlAddr + ' ' + str(e))
                else: # Not 403
                    printMessage('[-] ' + fnlAddr + ' ' + str(e))
        else:
            printMessage('[+] #' + str(counter + 1) + ' ' + fnlAddr + ' Succeeded')
            counter += 1

        prog.numScanned += 1
        progressBar()
        if not arg.argResult.isRapid:   # If delay set and it's not rapid
            time.sleep(arg.argResult.delay)

    printMessage('[*] Scan has finished with ' + str(counter) + ' relevant results')
    printMessage('[*] Keep your applications safe! Closing...')
    return 0


def init():
    currentOS = platform.system()
    if currentOS == "linux2" or currentOS == "darwin" or currentOS=="linux":    #Linux and Max OSX
        os.system('clear')
    elif currentOS == "win32" or currentOS == "cygwin"or currentOS == "Windows": #Windows
        os.system('cls')

    print '#######################################################'
    print '#  _     _       _                                    #'
    print '# (_)   (_)     | |                                   #'
    print '#  _     _ ____ | |__   ____ _____  ____ ___          #'
    print '# | |   | |    \|  _ \ / ___|____ |/ ___) _ \         #'
    print '# | |___| | | | | |_) ) |   / ___ ( (__| |_| |        #'
    print '#  \_____/|_|_|_|____/|_|   \_____|\____)___/         #'
    print '#         ______                                      #'
    print '#        / _____)                                     #'
    print '#       ( (____   ____ _____ ____  ____  _____  ____  #'
    print '#        \____ \ / ___|____ |  _ \|  _ \| ___ |/ ___) #'
    print '#        _____) | (___/ ___ | | | | | | | ____| |     #'
    print '#       (______/ \____)_____|_| |_|_| |_|_____)_|     #'
    print '#                                                     # '
    print '####################################################### '
    print '   # By:         Roei Jacobovich         #'
    print '   # Contact:    roei.jac@gmail.com      #'
    print '   #######################################'
    print '   # Remember: Attacking targets without #'
    print '   # permission is prohibited.           #'
    print '   # Developers are not responsible to   #'
    print '   # any illegal usage of this tool      #'
    print '   #######################################'
    print '\n'


def main():
    init()
    if arg.argControl() == 1:
        return 1    # Error

    #checkFiles()
    # Check address validation
    if arg.argResult.address[-1:] == '/':
        arg.argResult.address = arg.argResult.address[:-1]
    if arg.argResult.address[:7] != 'http://' or arg.argResult.address[:8] != 'https://':
        arg.argResult.address = 'http://' + arg.argResult.address

    if hasattr(arg,'file'):
        printMessage('[*] Logging into file...')

    scan(arg.argResult.address)
    arg.file.close()

if __name__ == "__main__":
    main()