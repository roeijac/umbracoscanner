import argparse

__author__ = 'admin'


class Progress:
    def __init__(self):
        self.numScanned = 0
        self.numAll = 0
        self.strAnimation = '.'
        self.msg = ''

    def refreshAnimation(self):
        if self.strAnimation == '.':
            self.strAnimation = '..'
        elif self.strAnimation == '..':
            self.strAnimation = '...'
        elif self.strAnimation == '...':
            self.strAnimation = '....'
        elif self.strAnimation == '....':
            self.strAnimation = '.....'
        elif self.strAnimation == '.....':
            self.strAnimation = '......'
        elif self.strAnimation == '......':
            self.strAnimation = '.'

class Arguments:
    def __init__(self):
        self.argResult = object()
    # Taking care of the arguments
    def argControl(self):
        parser = argparse.ArgumentParser(description='UmbracoScanner is a web application vulnerabilities '
                                                     + 'scanner, for Umbraco-based apps. UmbracoScanner will tell you'
                                                     + ' what is exposed to external users, and even tell you to replace'
                                                     + ' a specific file due to its vulnerability.')
        parser.add_argument('-u',dest='address',help='Address to scan')
        parser.add_argument('-d',dest='delay',default=1,help='Set delay (seconds) between requests (Default is 1 second)')
        parser.add_argument('--rapid',action='store_true',default=False,dest='isRapid',help='Disable delay, full speed '
                                                                                            + 'scanning')
        parser.add_argument('-V',dest='versionDetection',default=False,action='store_true',help='Version Detection - '
                                                                                                + 'Will try to find your '
                                                                                                + 'Umbraco version')
        parser.add_argument('-t',dest='threads',default=1,help='Set number of threads (Default is 1 thread)')
        parser.add_argument('-o',dest='logFile',help='Log data into file')
        parser.add_argument('--forbidden',dest='forbidden',default=False,action='store_true',help='Display HTTP Error '
                                                                                            + '403: Forbidden Results')

        self.argResult = parser.parse_args()
        if self.argResult.isRapid and self.argResult.delay != 1:
            print '[*] Argument Error:  Delay set and rapid set together. Closing...'
            return 1
        elif self.argResult.isRapid:
            self.argResult.delay = 0
        if not self.argResult.address:  # If user didn't enter address to scan
            print '[*] Argument Error:  No address to scan. Closing...'
            return 1
        if self.argResult.delay:    # Check delay parameter
            if self.argResult.delay < 0:
                print '[*] Argument Error:  Invalid delay. Closing...'
                return 1
        if self.argResult.logFile: # If log file defined
            self.file = open(self.argResult.logFile,mode='a')
            self.file.write("------------------ NEW LOG ------------------\n")